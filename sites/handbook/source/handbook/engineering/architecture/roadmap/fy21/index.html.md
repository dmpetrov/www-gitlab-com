---
layout: markdown_page
title: "FY21 Architecture Roadmap"
---

## FY21 Architecture Roadmap

1. [GitLab.com Migration to Cloud Native](https://about.gitlab.com/handbook/engineering/infrastructure/production/kubernetes/gitlab-com/)
   * [Cloud Native Build Logs](https://docs.gitlab.com/ee/architecture/blueprints/cloud_native_build_logs/)
   * [Cloud Native GitLab Pages](https://docs.gitlab.com/ee/architecture/blueprints/cloud_native_gitlab_pages/)
1. [Feature Flags for GitLab development](http://docs.gitlab.com/ee/architecture/blueprints/feature_flags_development/)
