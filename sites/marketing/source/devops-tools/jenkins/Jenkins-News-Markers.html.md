---
layout: markdown_page
title: "Jenkins News Markers"
description: "Jenkins News Markers - news from a variety of sources."
canonical_path: "/devops-tools/jenkins/Jenkins-News-Markers.html"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.



<!------------------Begin page additions below this line ------------------ -->


## HackerNews Thread - [Jenkins is Getting Old](https://news.ycombinator.com/item?id=19781251)

## Jenkinstein - From the article [DevOps World 2018: ‘Jenkinstein’ and a Cloud Native Jenkins](https://thenewstack.io/devops-world-2018-jenkinstein-and-a-cloud-native-jenkins/)
  - Describing the snowflake server and the ["Brent"](http://devopsdictionary.com/wiki/Brent) situation and how it slows down everything. This is one of the main issues which Jenkins users face today and which Jenkins has no easy fix for:
    > Acting as the gatekeeper for that channel is someone CloudBees CEO Sacha Labourey described as “the Jenkins guy… this superstar devoted to making Jenkins great on his team. Because this person is the authority on deployment within his organization, multiple teams come to rely on him to meet their scheduling goals. Yet this leads to a technical issue that few folks outside of IT operations take time to consider: The Jenkins Master . . . (the server managing a distributed scheme with multiple agents) becomes bloated, like an old telephone directory or the Windows XP System Registry.
    >
    > And because that organization’s Jenkins deployment is not only dependent upon the Guy, but somewhat bound to his choices of plug-ins, the result is what the CEO called “Frankenstein Jenkins,” and what other developers and engineers at the conference Tuesday had dubbed “Jenkinstein.”
  - Again, CloudBees CEO Sacha Labourey describing the common problem with current Jenkins:
    >  Jenkins becomes bloated, slow to start. When it crashes, it takes forever to start. Hundreds of developers are pissed. And nobody wants to fix it, because if you touch it, you own it, right?”

## [From note on 2018-08-31 from CloudBees CTO and Jenkins creator Kohsuke Kawaguchi](https://jenkins.io/blog/2018/08/31/shifting-gears/):
  - "Our Challenges. . . Service instability. . . Brittle configuration. . . Assembly required. . . Reduced development velocity. . ." (see above link for details on each)
  - "Path forward. . . Cloud Native Jenkins. . . continue the incremental evolution of Jenkins 2, but in an accelerated speed"
  - **Key takeaways**:
     - They plan on BREAKING backward compatibility in their upcoming releases
     - They plan on introducing a NEW flavor of Jenkins for Cloud native
     - If you’re a Jenkins user today, it’s going to be a rough ride ahead

## [From Jenkins Evergreen project page (identified in Kohsuke letter above as key for changes that need to come)](https://github.com/jenkinsci/jep/blob/master/jep/300/README.adoc)
   - "Pillars . . . Automatically Updated Distribution . . . Automatic Sane Defaults. . . Connected. . . Obvious Path to User Success"
   - "The "bucket of legos" approach is . . . not productive or useful for end-users [5] who are weighing their options between running Jenkins, or using a CI-as-a-Service offering such as Travis CI or Circle CI."
   - "existing processes around "Suggested Plugins", or any others for that matter, result in many "fiefdoms" of development rather than a shared understanding of problems and solutions which should be addressed to make new, and existing, users successful with Jenkins."

## [From "Problem" definition page of Jenkins Evergreen project page](https://github.com/jenkinsci/jep/blob/master/jep/300/README.adoc#problem):
   > For novice-to-intermediate users, the time necessary to prepare a Jenkins environment "from scratch" into something productive for common CI/CD workloads, can span from hours to days, depending on their understanding of Jenkins and it’s related technologies. The preparation of the environment can also be very error prone and require significant on-going maintenance overhead in order to continue to stay up-to-date, secure, and productive.
   >
   > Additionally, many Jenkins users suffer from a paradox of choice [6] when it comes to deciding which plugins should be combined, in which ways, and how they should be configured, in order to construct a suitable CI/CD environment for their projects. While this is related to the problem which JEP-2 [7] attempted to address in the "Setup Wizard" introduced in Jenkins 2.0, Jenkins Evergreen aims to address the broader problem of providing users with a low-overhead, easily maintained, and solid distribution of common features (provided by a set of existing plugins) which will help the user focus on building, testing, and delivering their projects rather than maintaining Jenkins.

## [Project analysis on Jenkins, pointed to by CloudBees documentation as proof for need of change](https://ghc.haskell.org/trac/ghc/wiki/ContinuousIntegration#Jenkins)
   > Pros
   >   - We can run build nodes on any architecture and OS we choose to set up.
   >
   > Cons
   >   - Security is low on PR builds unless we spend further effort to sandbox builds properly. Moreover, even with sandboxing, Jenkins security record is troublesome.
   >   - Jenkins is well known to be time consuming to set up.
   >   - Additional time spent setting up servers.
   >   - Additional time spent maintaining servers.
   >   - It is unclear how easy it is to make the set up reproducible.
   >   - The set up is not forkable (a forker would need to set up their own servers).
