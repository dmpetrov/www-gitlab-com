---
layout: markdown_page
title: "Azure DevOps Continuous Integration Overview"
description: "Find out how GitLab Continuous Integration compares to Azure DevOps Continuous Integration."
canonical_path: "/devops-tools/azure_devops/AzureDevOps-CI.html"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->


**Azure DevOps** provides an easy to use Marketplace of plugins/extensions which makes Azure DevOps’ extensibility prevalent at key places in the user experience, such as when authoring a pipeline. 

**GitLab** does not provide a marketplace of plugins for integrations into pipelines.

GitLab also offers Progressive Delivery capabilities such as Feature Flags, Review Apps and Deployment Scenarios (canar, incremental, etc.), which Azure DevOps does not offer.

How do we Compare:

|                                                                     | GitLab | Azure DevOps |
|---------------------------------------------------------------------|--------|--------------|
| Includes previous Release Manager (release pipelines)               |   Yes  |      Yes     |
| Native container support                                            |   Yes  |      Yes     |
| Save to any container registry                                      |   Yes  |      Yes     |
| Linux, Windows cloud hosted agents                                  |   Yes  |      Yes     |
|  macOS cloud hosted agents                                          |  Soon  |      Yes     |
| Deployment stages                                                   |   Yes  |      Yes     |
| Release gates, and approvals                                        |   No   |      Yes     |
| Maven, npm, and NuGet package feeds from public and private sources |   No   |      Yes     |
| Caching proxy of external repos/feeds                               |   No   |      Yes     |
| Artifacts integrate natively with pipelines                         |   Yes  |      Yes     |

