---
layout: markdown_page
title: "Comparison Methodology"
description: "GitLab recognizes that any comparison depends on the criteria and other baselines that are established. Learn more about our comparison methodology."
canonical_path: "/devops-tools/azure_devops/comparison-methodology.html"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->

##  Scoring GitLab and Other Vendor

GitLab recognizes that any comparison depends on the criteria and other baselines that are established.  To ensure that the scoring was fair and consistent, we took the following actions.

-  The comparison criteria was chosen as in the [GitLab Maturity](/direction/maturity/).  The following statement highlights why this is a fair benchmark.

Quoting from the maturity page  

*"GitLab has a broad scope and vision, and we are constantly iterating on existing and new features. Some stages and features are more mature than others. To convey the state of our feature set and be transparent, we have developed a maturity framework for categories, application types, and stages that considers both adoption and user experience. Contributions from our community are an essential part of achieving this overall vision for GitLab."*

This baseline set of criteria is realistic since it lays out a vision of what a complete end to end DevOps Application must include.  Furthermore, it is clear that GitLab also has some gaps to fill, which ensures that the criteria are fair.

-  We then established a baseline score for GitLab based on the capabilities and maturity in specific categories as laid out in the Maturity framework.

-  We then adjusted the competitor's score either upward or downward based on their capability level as it pertains to these categories.  This ensures that the scores are consistently applied consistently across all vendors.


## Color Coding Infographic Bars

Colors for the bars were based on the following score thresholds.
- Red:     0-25%
- Orange:  25-50%
- Green:   50-100%

## Highlighting Missing Functionality

Most of the score differences in this evaluation is attributed to missing capabilities in either GitLab or the other vendor's product.  Hence we thought it prudent to highlight those to give readers additional context for score differences between GitLab and other vendor.
